(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/ModalForgotPassword.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/components/ModalForgotPassword.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "ModalForgotPassword",
  data: function data() {
    return {
      formForgotPassword: {
        email: ""
      }
    };
  },
  methods: {
    forgotPassword: function forgotPassword() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _this.$store.dispatch("auth/forgotPassword", _this.formForgotPassword);

              case 2:
                response = _context.sent;

                if (!(response.status == 422)) {
                  _context.next = 6;
                  break;
                }

                _this.$refs.form.setErrors(response.data.errors);

                return _context.abrupt("return");

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    hideModal: function hideModal() {
      this.$store.dispatch("auth/showModalPassword", false);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/components/header/LoginPopover.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/layouts/components/header/LoginPopover.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_ModalForgotPassword__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/components/ModalForgotPassword */ "./resources/js/src/components/ModalForgotPassword.vue");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "LoginForm",
  components: {
    ModalForgotPassword: _components_ModalForgotPassword__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  data: function data() {
    return {
      show: false,
      form: {
        email: "",
        password: "",
        remember: false
      }
    };
  },
  methods: {
    login: function login() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _this.$store.dispatch("auth/login", _this.form);

              case 2:
                response = _context.sent;

                if (!(response.status == 422)) {
                  _context.next = 6;
                  break;
                }

                _this.$refs.form.setErrors(response.data.errors);

                return _context.abrupt("return");

              case 6:
                if (!(_this.$router.currentRoute.path == "/login")) {
                  _context.next = 8;
                  break;
                }

                return _context.abrupt("return", _this.$router.replace("/"));

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    socialLogin: function socialLogin(socialNetwork) {
      var response = this.$store.dispatch("auth/socialLogin", socialNetwork);
      return window.location.replace("".concat(response.data));
    },
    showModalPassword: function showModalPassword() {
      this.$store.dispatch("auth/showModalPassword", true);
    }
  },
  computed: {
    isAuth: function isAuth() {
      return this.$store.state.loading;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/ModalForgotPassword.vue?vue&type=template&id=707f9c5b&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/components/ModalForgotPassword.vue?vue&type=template&id=707f9c5b& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("ValidationObserver", {
    ref: "form",
    scopedSlots: _vm._u([
      {
        key: "default",
        fn: function(ref) {
          var invalid = ref.invalid
          var validated = ref.validated
          var passes = ref.passes
          return [
            _c(
              "v-dialog",
              {
                attrs: { "max-width": "50%" },
                model: {
                  value: _vm.$store.state.auth.modalPassword,
                  callback: function($$v) {
                    _vm.$set(_vm.$store.state.auth, "modalPassword", $$v)
                  },
                  expression: "$store.state.auth.modalPassword"
                }
              },
              [
                _c(
                  "v-card",
                  [
                    _c("v-card-title", { staticClass: "headline" }),
                    _vm._v(" "),
                    _c(
                      "v-list",
                      [
                        _c(
                          "v-list-item",
                          [
                            _c(
                              "v-list-item-content",
                              [
                                _c(
                                  "v-list-item-title",
                                  { staticClass: "text-center" },
                                  [_vm._v("¿Olvidaste tú contraseña?")]
                                )
                              ],
                              1
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "v-list-item",
                          [
                            _c(
                              "v-list-item-content",
                              [
                                _c(
                                  "v-form",
                                  [
                                    _c("ValidationProvider", {
                                      attrs: {
                                        vid: "email",
                                        name: "CORREO ELECTRONICO",
                                        rules: "required|email"
                                      },
                                      scopedSlots: _vm._u(
                                        [
                                          {
                                            key: "default",
                                            fn: function(ref) {
                                              var errors = ref.errors
                                              var valid = ref.valid
                                              return [
                                                _c("v-text-field", {
                                                  attrs: {
                                                    outlined: "",
                                                    rounded: "",
                                                    dense: "",
                                                    label: "CORREO ELECTRONICO",
                                                    "error-messages": errors,
                                                    success: valid,
                                                    loading:
                                                      _vm.$store.state.auth
                                                        .modalLoading,
                                                    disabled:
                                                      _vm.$store.state.auth
                                                        .modalLoading
                                                  },
                                                  model: {
                                                    value:
                                                      _vm.formForgotPassword
                                                        .email,
                                                    callback: function($$v) {
                                                      _vm.$set(
                                                        _vm.formForgotPassword,
                                                        "email",
                                                        $$v
                                                      )
                                                    },
                                                    expression:
                                                      "formForgotPassword.email"
                                                  }
                                                })
                                              ]
                                            }
                                          }
                                        ],
                                        null,
                                        true
                                      )
                                    })
                                  ],
                                  1
                                )
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c("v-divider"),
                    _vm._v(" "),
                    _c(
                      "v-card-actions",
                      { staticClass: "d-flex justify-center" },
                      [
                        _c(
                          "v-btn",
                          {
                            attrs: {
                              disabled: _vm.$store.state.auth.modalLoading,
                              color: "grey darken-1",
                              text: ""
                            },
                            on: { click: _vm.hideModal }
                          },
                          [_vm._v("Cerrar")]
                        ),
                        _vm._v(" "),
                        _c(
                          "v-btn",
                          {
                            attrs: {
                              color: "green darken-1",
                              text: "",
                              disabled:
                                invalid ||
                                !validated ||
                                _vm.$store.state.auth.modalLoading,
                              loading: _vm.$store.state.auth.modalLoading
                            },
                            on: {
                              click: function($event) {
                                return passes(_vm.forgotPassword)
                              }
                            }
                          },
                          [_vm._v("Validar")]
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            )
          ]
        }
      }
    ])
  })
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/components/header/LoginPopover.vue?vue&type=template&id=f3f97898&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/layouts/components/header/LoginPopover.vue?vue&type=template&id=f3f97898& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("ValidationObserver", {
        ref: "form",
        scopedSlots: _vm._u([
          {
            key: "default",
            fn: function(ref) {
              var invalid = ref.invalid
              var validated = ref.validated
              var passes = ref.passes
              return [
                _c(
                  "v-card",
                  [
                    _c(
                      "v-overlay",
                      { attrs: { absolute: _vm.isAuth, value: _vm.isAuth } },
                      [
                        _c("v-progress-circular", {
                          attrs: { indeterminate: "", size: "64" }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "v-list",
                      [
                        _c(
                          "v-list-item",
                          [
                            _c(
                              "v-list-item-content",
                              [
                                _c(
                                  "v-list-item-title",
                                  { staticClass: "text-center" },
                                  [_vm._v(_vm._s(_vm.$store.state.appTitle))]
                                ),
                                _vm._v(" "),
                                _c(
                                  "v-list-item-subtitle",
                                  { staticClass: "text-center" },
                                  [_vm._v("Iniciar sesión")]
                                )
                              ],
                              1
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "v-list-item",
                          [
                            _c(
                              "v-list-item-content",
                              [
                                _c(
                                  "v-form",
                                  [
                                    _c("ValidationProvider", {
                                      attrs: {
                                        vid: "email",
                                        name: "CORREO ELECTRONICO",
                                        rules: "required|email"
                                      },
                                      scopedSlots: _vm._u(
                                        [
                                          {
                                            key: "default",
                                            fn: function(ref) {
                                              var errors = ref.errors
                                              var valid = ref.valid
                                              return [
                                                _c("v-text-field", {
                                                  attrs: {
                                                    outlined: "",
                                                    rounded: "",
                                                    dense: "",
                                                    label: "CORREO ELECTRONICO",
                                                    "error-messages": errors,
                                                    success: valid,
                                                    disabled:
                                                      _vm.$store.state.loading
                                                  },
                                                  model: {
                                                    value: _vm.form.email,
                                                    callback: function($$v) {
                                                      _vm.$set(
                                                        _vm.form,
                                                        "email",
                                                        $$v
                                                      )
                                                    },
                                                    expression: "form.email"
                                                  }
                                                })
                                              ]
                                            }
                                          }
                                        ],
                                        null,
                                        true
                                      )
                                    }),
                                    _vm._v(" "),
                                    _c("ValidationProvider", {
                                      attrs: {
                                        vid: "password",
                                        name: "CONTRASEÑA",
                                        rules: "required|min:8"
                                      },
                                      scopedSlots: _vm._u(
                                        [
                                          {
                                            key: "default",
                                            fn: function(ref) {
                                              var errors = ref.errors
                                              var valid = ref.valid
                                              return [
                                                _c("v-text-field", {
                                                  attrs: {
                                                    outlined: "",
                                                    rounded: "",
                                                    dense: "",
                                                    label: "CONTRASEÑA",
                                                    "append-icon": _vm.show
                                                      ? "fas fa-eye"
                                                      : "fas fa-eye-slash",
                                                    type: _vm.show
                                                      ? "text"
                                                      : "password",
                                                    counter: 8,
                                                    "error-messages": errors,
                                                    success: valid,
                                                    disabled:
                                                      _vm.$store.state.loading
                                                  },
                                                  on: {
                                                    "click:append": function(
                                                      $event
                                                    ) {
                                                      _vm.show = !_vm.show
                                                    }
                                                  },
                                                  model: {
                                                    value: _vm.form.password,
                                                    callback: function($$v) {
                                                      _vm.$set(
                                                        _vm.form,
                                                        "password",
                                                        $$v
                                                      )
                                                    },
                                                    expression: "form.password"
                                                  }
                                                })
                                              ]
                                            }
                                          }
                                        ],
                                        null,
                                        true
                                      )
                                    }),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "d-flex align-center justify-center"
                                      },
                                      [
                                        _c("v-checkbox", {
                                          attrs: {
                                            label: "Rercuerdame",
                                            value: "true",
                                            disabled: _vm.$store.state.loading
                                          },
                                          model: {
                                            value: _vm.form.remember,
                                            callback: function($$v) {
                                              _vm.$set(
                                                _vm.form,
                                                "remember",
                                                $$v
                                              )
                                            },
                                            expression: "form.remember"
                                          }
                                        }),
                                        _vm._v(" "),
                                        _c(
                                          "v-btn",
                                          {
                                            staticClass: "ml-1",
                                            attrs: {
                                              disabled:
                                                _vm.$store.state.loading,
                                              text: "",
                                              small: ""
                                            },
                                            on: {
                                              click: function($event) {
                                                $event.stopPropagation()
                                                return _vm.showModalPassword(
                                                  $event
                                                )
                                              }
                                            }
                                          },
                                          [_vm._v("¿Olvidaste tú contraseña?")]
                                        )
                                      ],
                                      1
                                    )
                                  ],
                                  1
                                )
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c("v-divider"),
                    _vm._v(" "),
                    _c(
                      "v-card-actions",
                      { staticClass: "d-flex justify-center" },
                      [
                        _c(
                          "v-btn",
                          {
                            attrs: {
                              to: "/registro",
                              color: "primary",
                              text: ""
                            }
                          },
                          [_vm._v("¡Registrate Gratis!")]
                        ),
                        _vm._v(" "),
                        _c(
                          "v-btn",
                          {
                            attrs: {
                              disabled: invalid || !validated,
                              color: "primary",
                              text: ""
                            },
                            on: {
                              click: function($event) {
                                return passes(_vm.login)
                              }
                            }
                          },
                          [_vm._v("Login")]
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ]
            }
          }
        ])
      }),
      _vm._v(" "),
      _c("modal-forgot-password")
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/components/ModalForgotPassword.vue":
/*!*************************************************************!*\
  !*** ./resources/js/src/components/ModalForgotPassword.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ModalForgotPassword_vue_vue_type_template_id_707f9c5b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ModalForgotPassword.vue?vue&type=template&id=707f9c5b& */ "./resources/js/src/components/ModalForgotPassword.vue?vue&type=template&id=707f9c5b&");
/* harmony import */ var _ModalForgotPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ModalForgotPassword.vue?vue&type=script&lang=js& */ "./resources/js/src/components/ModalForgotPassword.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ModalForgotPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ModalForgotPassword_vue_vue_type_template_id_707f9c5b___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ModalForgotPassword_vue_vue_type_template_id_707f9c5b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/components/ModalForgotPassword.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/components/ModalForgotPassword.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/src/components/ModalForgotPassword.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ModalForgotPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ModalForgotPassword.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/ModalForgotPassword.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ModalForgotPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/components/ModalForgotPassword.vue?vue&type=template&id=707f9c5b&":
/*!********************************************************************************************!*\
  !*** ./resources/js/src/components/ModalForgotPassword.vue?vue&type=template&id=707f9c5b& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ModalForgotPassword_vue_vue_type_template_id_707f9c5b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ModalForgotPassword.vue?vue&type=template&id=707f9c5b& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/ModalForgotPassword.vue?vue&type=template&id=707f9c5b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ModalForgotPassword_vue_vue_type_template_id_707f9c5b___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ModalForgotPassword_vue_vue_type_template_id_707f9c5b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/layouts/components/header/LoginPopover.vue":
/*!*********************************************************************!*\
  !*** ./resources/js/src/layouts/components/header/LoginPopover.vue ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _LoginPopover_vue_vue_type_template_id_f3f97898___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./LoginPopover.vue?vue&type=template&id=f3f97898& */ "./resources/js/src/layouts/components/header/LoginPopover.vue?vue&type=template&id=f3f97898&");
/* harmony import */ var _LoginPopover_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./LoginPopover.vue?vue&type=script&lang=js& */ "./resources/js/src/layouts/components/header/LoginPopover.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _LoginPopover_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _LoginPopover_vue_vue_type_template_id_f3f97898___WEBPACK_IMPORTED_MODULE_0__["render"],
  _LoginPopover_vue_vue_type_template_id_f3f97898___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/layouts/components/header/LoginPopover.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/layouts/components/header/LoginPopover.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/src/layouts/components/header/LoginPopover.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_LoginPopover_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./LoginPopover.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/components/header/LoginPopover.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_LoginPopover_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/layouts/components/header/LoginPopover.vue?vue&type=template&id=f3f97898&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/src/layouts/components/header/LoginPopover.vue?vue&type=template&id=f3f97898& ***!
  \****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_LoginPopover_vue_vue_type_template_id_f3f97898___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./LoginPopover.vue?vue&type=template&id=f3f97898& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/components/header/LoginPopover.vue?vue&type=template&id=f3f97898&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_LoginPopover_vue_vue_type_template_id_f3f97898___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_LoginPopover_vue_vue_type_template_id_f3f97898___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);