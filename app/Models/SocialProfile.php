<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class SocialProfile extends Model
{
    protected $guarded = [];

    public static $allowed = ['facebook', 'strava',];

    /**
     * Funcion que retorna el usuario de la red social
     *
     * @author Sergio Benavides
     *
     * @return App\User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
