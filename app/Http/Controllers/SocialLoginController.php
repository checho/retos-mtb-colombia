<?php

namespace App\Http\Controllers;

use App\User;
use Socialite;
use Illuminate\Http\Request;
use App\Models\SocialProfile;
use Illuminate\Support\Facades\Auth;

class SocialLoginController extends Controller
{
    public function redirectToSocialNetwork($socialNetwork)
    {
        $redirect = Socialite::with($socialNetwork)->redirect();
        // $redirect = Socialite::driver($socialNetwork)->stateless()->user();

        return response()->json($redirect->headers->get('location'));
    }

    public function handleSocialNetworkCallback($socialNetwork)
    {
        try {
            $socialUser = Socialite::driver($socialNetwork)->user();

            dd($socialUser);
        } catch (\Exception $e) {
            return redirect()->route('login')->with('warning', 'Hubo un error en el login...');
        }

        $socialProfile = SocialProfile::firstOrNew([
            'social_network' => $socialNetwork,
            'social_network_user_id' => $socialUser->getId()
        ]);

        if (!$socialProfile->exists) {
            $user = User::firstOrNew(['email' => $socialUser->getEmail()]);

            if (!$user->exists) {
                $user->name = $socialUser->getName();
                $user->save();
            }

            $socialProfile->avatar = $socialUser->getAvatar();

            $user->profiles()->save($socialProfile);
        }

        Auth::login($socialProfile->user);

        return response()->json($socialProfile);
    }
}
