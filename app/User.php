<?php

namespace App;

use App\Models\SocialProfile;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Relationships

    /**
     * Funcion que retorna los perfiles de redes sociales
     *
     * @author Sergio Benavides
     *
     * @return app\Models\SocialProfile
     */
    public function profiles()
    {
        return $this->hasMany(SocialProfile::class);
    }

    public function getAvatarAttribute()
    {
        return optional( $this->profiles->last() )->avatar ?? url('images/default.jpg');
    }
}
