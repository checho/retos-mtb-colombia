/*=========================================================================================
  File Name: globalComponents.js
  Description: aca se van a registrar todos los componentes globales
  ----------------------------------------------------------------------------------------
  Item Name: retos mtb colombia
  Author: Sergio Benavides
==========================================================================================*/


import Vue from 'vue'
// import VxTooltip from './layouts/components/vx-tooltip/VxTooltip.vue'
// import VxCard  from './components/vx-card/VxCard.vue'
// import VxList  from './components/vx-list/VxList.vue'
// import VxBreadcrumb  from './layouts/components/VxBreadcrumb.vue'
// import FeatherIcon  from './components/FeatherIcon.vue'
// import VxInputGroup  from './components/vx-input-group/VxInputGroup.vue'
import SuccessMessage from './components/SuccessMessage.vue'

Vue.component(SuccessMessage.name, SuccessMessage)
// Vue.component(VxTooltip.name, VxTooltip)
// Vue.component(VxCard.name, VxCard)
// Vue.component(VxList.name, VxList)
// Vue.component(VxBreadcrumb.name, VxBreadcrumb)
// Vue.component(FeatherIcon.name, FeatherIcon)
// Vue.component(VxInputGroup.name, VxInputGroup)
