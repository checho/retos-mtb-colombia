/*=========================================================================================
  File Name: state.js
  Description: Vuex Store - state
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/



// /////////////////////////////////////////////
// Variables
// /////////////////////////////////////////////

const userDefaults = {
  //
}

// /////////////////////////////////////////////
// State
// /////////////////////////////////////////////

const state = {
  currentTab: 0,
  tabs: [
    {
      disabled: false,
      tab: "DATOS PERSONALES",
      component: "InscriptionFormPersonalData",
    },
    {
      disabled: true,
      tab: "DATOS DE PARTICIPACIÓN",
      component: "InscriptionFormParticipationData",
    },
    {
      disabled: true,
      tab: "TIPO DE INSCRIPCIÓN",
      component: "InscriptionFormInscriptionTypeData",
    },
    {
      disabled: true,
      tab: "DATOS DE ENVIÓ",
      component: "InscriptionFormDeliveryData",
    },
  ],
}

export default state
