
/*=========================================================================================
File Name: mutations.js
Description: Vuex Store - mutations
----------------------------------------------------------------------------------------
Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
Author: Pixinvent
Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


const mutations = {

    UPDATE_STATUS_TAB(state, data) {
        if (data.next === true) {
            const nextTab = state.tabs[data.currentTab + 1]
            nextTab.disabled = false
            state.currentTab = data.currentTab + 1
        } else {
            state.currentTab = data.currentTab - 1
        }
    }

}

export default mutations

