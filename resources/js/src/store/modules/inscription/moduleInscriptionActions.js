/*=========================================================================================
  File Name: actions.js
  Description: Vuex Store - actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
import axios from "@/plugins/axios"

const actions = {

  // /////////////////////////////////////////////
  // COMPONENTS
  // /////////////////////////////////////////////



  // /////////////////////////////////////////////
  // UI
  // /////////////////////////////////////////////

  async nextOrPrevTab(context, payload) {
    try {
      await context.commit('UPDATE_STATUS_TAB', payload)
    } catch (error) {
      console.log({ err: error });
    }
  },


}

export default actions
