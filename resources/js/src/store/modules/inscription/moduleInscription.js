import state from './moduleInscriptionState'
import mutations from "./moduleInscriptionMutations"
import actions from "./moduleInscriptionActions"
import getters from "./moduleInscriptionGetters"

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}