/*=========================================================================================
  File Name: store.js
  Description: Vuex store
  ----------------------------------------------------------------------------------------
  Item Name: retos mtb colombia
  Author: Sergio Benavides
==========================================================================================*/


import Vue from 'vue'
import Vuex from 'vuex'

import state from "./state"
import getters from "./getters"
import mutations from "./mutations"
import actions from "./actions"

Vue.use(Vuex)

import moduleAuth from './modules/auth/moduleAuth'
import moduleInscription from './modules/inscription/moduleInscription'

export default new Vuex.Store({
    getters,
    mutations,
    state,
    actions,
    modules: {
      auth: moduleAuth,
      inscription: moduleInscription,
    },
    strict: process.env.NODE_ENV !== 'production'
})
