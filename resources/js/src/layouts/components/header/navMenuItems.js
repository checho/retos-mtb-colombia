/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  ----------------------------------------------------------------------------------------
  Item Name: retos mtb colombia
  Author: Sergio benavides
==========================================================================================*/


export default [

  {
    title: "RETO MARIQUITA - LETRAS",
    path: "",
    icon: "fas fa-mountain",
    childrens: [
      {
        title: "Información",
        path: "/informacion",
        icon: "fas fa-info-circle"
      },
      {
        title: "Inscripción",
        path: "/inscripcion",
        icon: "fas fa-clipboard-list"
      },
      {
        title: "Valida tu inscripción",
        path: "/validar-inscripcion",
        icon: "fas fa-clipboard-check"
      }
    ],
    popover: false
  },
  {
    title: "EVENTOS",
    path: "",
    icon: "fas fa-info-circle",
    childrens: [
      {
        title: "EVENTO 1",
        path: "/evento-1",
        icon: "fas fa-info-circle"
      },
      {
        title: "EVENTO 2",
        path: "/evento-2",
        icon: "fas fa-clipboard-list"
      },
      {
        title: "EVENTO 3",
        path: "/evento-3",
        icon: "fas fa-clipboard-check"
      }
    ],
    popover: false
  },
  {
    title: "TIENDA",
    path: "/tienda",
    icon: "fas fa-store-alt",
    childrens: [
      {
        title: "Información",
        path: "/informacion",
        icon: "fas fa-info-circle"
      },
      {
        title: "Inscripción",
        path: "/inscripcion",
        icon: "fas fa-clipboard-list"
      },
      {
        title: "Valida tu inscripción",
        path: "/validar-inscripcion",
        icon: "fas fa-clipboard-check"
      }
    ],
    popover: false
  },
  {
    title: "CONTACTO",
    path: "/contactanos",
    icon: "fas fa-envelope",
    childrens: [],
    popover: false
  },
  {
    title: "INICIA SESION",
    path: "",
    icon: "fas fa-unlock-alt",
    childrens: [],
    popover: true,
    component: "Login"
  }
]
